# `rules_gzip`

> A Bazel ruleset around creating tape archives (`gzip`)

## Getting Started

Add the following to `MODULE.bazel`:

```py
bazel_dep(name="rules_gzip", version="0.0.0")
```

Use the compression and decompression rules:

```py
load("@rules_gzip//gzip/compress:defs.bzl", "gzip_compress")
load("@rules_gzip//gzip/decompress:defs.bzl", "gzip_decompress")

gzip_compress(
    name = "compress",
    src = "//fixture:hello-world.txt",
)

gzip_decompress(
    name = "decompress",
    src = ":compress",
)
```

# Hermeticity

The module is entirely hermetic, using `@ape//ape/toolchain/info:gzip` as the default toolchain.

## Release Registry

The project publishes the relevant files to GitLab releases for use when a version has not been added to the upstream [BCR][bcr].

This is often the case for pre-release versions.

Add the following to `.bazelrc`:

```
# `bzlmod` pre-release registries
common --registry https://bcr.bazel.build
common --registry=https://gitlab.arm.com/bazel/rules_gzip/-/releases/v1.0.0-alpha.1/downloads
```

Then a GitLab release version can be used in `bazel_dep`.

[bcr]: https://registry.bazel.build/
