visibility("//gzip/...")

DOC = """A DEFLATE compressed file to be decompressed.

```py
gzip_decompress(
    name = "decompress",
    src = ":archive.gz",
)
```
"""

ATTRS = {
    "src": attr.label(
        doc = "Decompresses a DEFLATE compressed file.",
        mandatory = True,
        allow_single_file = [".gz"],
    ),
    "_template": attr.label(
        default = "@rules_coreutils//coreutils/redirect:stdout",
        allow_single_file = True,
    ),
}

def implementation(ctx):
    gzip = ctx.toolchains["//gzip/toolchain/gzip:type"]

    output = ctx.actions.declare_file(ctx.file.src.basename.removesuffix(".gz"))
    rendered = ctx.actions.declare_file("{}.{}".format(ctx.label.name, ctx.file._template.extension))

    ctx.actions.expand_template(
        template = ctx.file._template,
        output = rendered,
        is_executable = True,
        substitutions = {
            "{{stdout}}": output.path,
        },
    )
    args = ctx.actions.args()
    args.add(gzip.executable.path)
    args.add("-cdfk").add(ctx.file.src)

    ctx.actions.run(
        outputs = [output],
        inputs = [ctx.file.src],
        arguments = [args],
        tools = [gzip.run],
        executable = rendered,
        toolchain = "//gzip/toolchain/gzip:type",
        mnemonic = "GzipDecompress",
        progress_message = "Decompressing %{input} to %{output}",
    )

    return DefaultInfo(files = depset([output]))

gzip_decompress = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
    toolchains = ["//gzip/toolchain/gzip:type"],
)

decompress = gzip_decompress
