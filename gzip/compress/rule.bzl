visibility("//gzip/...")

DOC = """Compresses a file wth the DEFLATE algorithm into a `gzip` format file.

```py
gzip_compress(
    name = "compress",
    src = "some/file.txt",
    level = 1-9
)
```
"""

ATTRS = {
    "src": attr.label(
        doc = "A file to compress with the DEFLATE algorithm.",
        mandatory = True,
        allow_single_file = True,
    ),
    "level": attr.int(
        doc = "The compression level.",
        mandatory = False,
        default = 6,
        values = [l for l in range(1, 10)],
    ),
    "_template": attr.label(
        default = "@rules_coreutils//coreutils/redirect:stdout",
        allow_single_file = True,
    ),
}

def implementation(ctx):
    gzip = ctx.toolchains["//gzip/toolchain/gzip:type"]

    output = ctx.actions.declare_file("{}.gz".format(ctx.file.src.basename))
    rendered = ctx.actions.declare_file("{}.{}".format(ctx.label.name, ctx.file._template.extension))

    ctx.actions.expand_template(
        template = ctx.file._template,
        output = rendered,
        is_executable = True,
        substitutions = {
            "{{stdout}}": output.path,
        },
    )
    args = ctx.actions.args()
    args.add(gzip.executable.path)
    args.add("-cfk").add(ctx.file.src)
    args.add("-{}".format(ctx.attr.level))

    ctx.actions.run(
        outputs = [output],
        inputs = [ctx.file.src],
        arguments = [args],
        tools = [gzip.run],
        executable = rendered,
        toolchain = "//gzip/toolchain/gzip:type",
        mnemonic = "GzipCompress",
        progress_message = "Compressing %{input} to %{output}",
    )

    return DefaultInfo(files = depset([output]))

gzip_compress = rule(
    doc = DOC,
    attrs = ATTRS,
    implementation = implementation,
    toolchains = ["//gzip/toolchain/gzip:type"],
)

compress = gzip_compress
